var mongo = require('mongoskin');
var Promise = require('bluebird');

var db = mongo.db("mongodb://localhost:27017/users", {native_parser:true});

var findByName = function(name) {
	return new Promise(function (resolve, reject) {
		db.collection('users').findOne({'user':name}, function(error, result) {
		    if(error) {
		    	reject(error);
		    } else {
		    	resolve(result);
		    }
		});
	});
}

module.exports ={findByName:findByName};