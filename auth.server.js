var express = require('express');
var bodyParser = require('body-parser');

var jwt = require('jsonwebtoken');  //https://npmjs.org/package/node-jsonwebtoken
var expressJwt = require('express-jwt'); //https://npmjs.org/package/express-jwt


var secret = 'this is the secret secret secret 12356';

var app = express();

var util = require('util');

var userService = require('./userService');


// We are going to protect /api routes with JWT
app.use('/api', expressJwt({secret: secret}));

app.use(bodyParser.json());
app.use('/', express.static(__dirname + '/'));

app.use(function(err, req, res, next){
  if (err.constructor.name === 'UnauthorizedError') {
    res.status(401).send('Unauthorized');
  } else {
    console.log(err);
res.end();
  }
});

app.post('/authenticate', function (req, res) {
  userService.findByName(req.body.username).then(function(result) {
    console.log("Resultado : " + JSON.stringify(result));
//TODO validate req.body.username and req.body.password
  //if is invalid, return 401
  if (!(result.user === 'carlos' && req.body.password === 'foobar')) {
    res.status(401).send('Wrong user or password');
    return;
  }

  var profile = {
    first_name: 'John',
    last_name: 'Doe',
    email: 'john@doe.com',
    id: 123
  };

  // We are sending the profile inside the token
  var token = jwt.sign(profile, secret, { expiresInMinutes: 60*5 });

  res.json({ token: token });

  });
});

app.get('/api/restricted', function (req, res) {
  console.log('user ' + req.user.email + ' is calling /api/restricted');
  res.json({
    name: 'foo'
  });
});

app.post('/api/posting', function (req, res) {
  console.log('user ' + req.user.email + ' is calling /api/restricted con POST');
  res.json({
    name: 'tu_vieja'
  });
});


app.get('/api/testing', function (req, res) {
console.log("AMigo me re pegan");
  res.end();
});

app.listen(8080, function () {
  console.log('listening on http://localhost:8080');
});
