
var usersCollection = require('./mongoskinUser');

var findByNameService = function(name) {
	return usersCollection.findByName(name).then(function(result) {
    	setTimeout(function(){
			console.log("Resultado atrazado async" + JSON.stringify(result));
    	},10000);
    	return result;
  	});
}

module.exports ={findByName:findByNameService};